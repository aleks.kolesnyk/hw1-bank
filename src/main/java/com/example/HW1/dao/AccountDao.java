package com.example.HW1.dao;

import com.example.HW1.domain.Account;
import com.example.HW1.domain.Currency;
import com.example.HW1.domain.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class AccountDao implements Dao<Account> {

    List<Account> accountsList = List.of(
            new Account(Currency.UAH, new Customer("Alex", "alex@mail.com", 20)),
            new Account(Currency.EUR, new Customer("Mary", "mary@mail.con", 22)));

    @Override
    public Account save(Account account) {
        accountsList.add(account);
        return account;
    }

    @Override
    public boolean delete(Account account) {
        if (accountsList.contains(account)){
            accountsList.remove(account);
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void deleteAll(List<Account> accounts) {
        accountsList.removeAll(accounts);
    }

    @Override
    public void saveAll(List<Account> accounts) {
        accountsList.addAll(accounts);
    }

    @Override
    public List<Account> findAll() {

        return accountsList;
    }

    @Override
    public boolean deleteById(long id) {

        return false;
    }

    @Override
    public Account getOne(long id) {

        return null;
    }

    public void update(Account account) {
        accountsList.add(account);
    }
}
