package com.example.HW1.dao;

import com.example.HW1.domain.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public class CustomerDao implements Dao<Customer> {

    List<Customer> customerList = List.of(
            new Customer("Alex", "alex@mail.com", 20),
            new Customer("Mary", "mary@mail.con", 22));

    @Override
    public Customer save(Customer customer) {
        customerList.add(customer);
        return customer;
    }

    @Override
    public boolean delete(Customer customer) {
        if (customerList.contains(customer)){
            customerList.remove(customer);
            return true;
        }else {
            return false;
        }
    }

    @Override
    public void deleteAll(List<Customer> customers) {
        customerList.removeAll(customers);
    }

    @Override
    public void saveAll(List<Customer> customers) {
        customerList.addAll(customers);
    }

    @Override
    public List<Customer> findAll() {

        return customerList;
    }

    @Override
    public boolean deleteById(long id) {

        return false;
    }

    @Override
    public Customer getOne(long id) {
        for (Customer customer : customerList) {
            if (customer.getId().equals(id)) {
                return customer;
            }
        }
        return null;
    }

    public void edit(Customer customer) {
        Customer customerForEdit = getOne(customer.getId());
        customerForEdit.setAge(customer.getAge());
        customerForEdit.setEmail(customer.getEmail());
        customerForEdit.setName(customer.getName());
        customerForEdit.setAccounts(customer.getAccounts());
    }

    public void addAccount(){

    }
}
