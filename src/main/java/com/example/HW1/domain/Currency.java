package com.example.HW1.domain;

public enum Currency {
    USD, EUR, UAH, CHF, GBP;
}
