package com.example.HW1.resource;

import com.example.HW1.domain.Account;
import com.example.HW1.service.AccountService;
import com.example.HW1.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/accounts")
public class RestAccountController {

    private final AccountService accountService;
    private final CustomerService customerService;

    public RestAccountController(AccountService accountService, CustomerService customerService) {

        this.accountService = accountService;
        this.customerService = customerService;
    }


    @GetMapping
    public List<Account> findAll() {

        return accountService.findAll();
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Account account) {

        try {
            accountService.save(account);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body("Cant save new account");
        }
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Account account) {

        try {
            accountService.delete(account);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body("Cant delete account");
        }
    }

    //Think about this method
    @DeleteMapping("/acc")
    public ResponseEntity<?> deleteAll() {

        try {
            List<Account> accounts = accountService.findAll();
            accountService.deleteAll(accounts);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body("Cant delete accounts");
        }
    }

    //Think about this method
    @PostMapping("/all")
    public ResponseEntity<?> saveAll() {

        try {
            List<Account> accounts = accountService.findAll();
            accountService.saveAll(accounts);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body("Cant save accounts");
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {

        try {
            accountService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body("Cant delete account");
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") Long id) {

        try {
            accountService.getOne(id);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body("Cant find account");
        }
    }

    @PutMapping("/balance")
    public ResponseEntity<?> addMoney(@RequestParam UUID number, @RequestParam Double amount) {

        Optional<Account> accountOptional = accountService.findAll()
                .stream().filter(el -> el.getNumber().equals(number.toString()))
                .findAny();

        if (accountOptional.isEmpty()) {
            return ResponseEntity.badRequest().body("Account not found");
        }

        Account editedAccount = accountOptional.get();
        Double accountBalance = accountOptional.get().getBalance();
        Double accountNewBalance = accountBalance + amount;
        editedAccount.setBalance(accountNewBalance);
        accountService.update(editedAccount);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/balance")
    public ResponseEntity<?> takeMoney(@RequestParam UUID number, @RequestParam Double amount) {

        Optional<Account> accountOptional = accountService.findAll()
                .stream().filter(el -> el.getNumber().equals(number.toString()))
                .findAny();

        if (accountOptional.isEmpty()) {
            return ResponseEntity.badRequest().body("Account not found");
        }

        Account editedAccount = accountOptional.get();
        Double accountBalance = accountOptional.get().getBalance();
        if (accountBalance - amount < 0) {
            return ResponseEntity.badRequest().body("There is not enough money on your account");
        }

        Double accountNewBalance = accountBalance - amount;
        editedAccount.setBalance(accountNewBalance);
        accountService.update(editedAccount);

        return ResponseEntity.ok().build();
    }

    @PutMapping("/balance/transaction")
    public ResponseEntity<?> transactionMoney(@RequestParam UUID numberAccountFrom, @RequestParam UUID numberAccountTo, @RequestParam Double amount) {

        Optional<Account> accountFromOptional = accountService.findAll()
                .stream().filter(el -> el.getNumber().equals(numberAccountFrom.toString()))
                .findAny();

        if (accountFromOptional.isEmpty()) {
            return ResponseEntity.badRequest().body("Account not found");
        }

        Optional<Account> accountToOptional = accountService.findAll()
                .stream().filter(el -> el.getNumber().equals(numberAccountTo.toString()))
                .findAny();

        if (accountToOptional.isEmpty()) {
            return ResponseEntity.badRequest().body("Account not found");
        }

        if (accountFromOptional.get().getBalance() - amount < 0) {

            return ResponseEntity.badRequest().body("Not enough money on account");
        }
        takeMoney(numberAccountFrom, amount);
        addMoney(numberAccountTo, amount);

        return ResponseEntity.ok().build();

    }


}
