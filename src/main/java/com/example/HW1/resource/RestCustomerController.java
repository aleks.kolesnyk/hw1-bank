package com.example.HW1.resource;

import com.example.HW1.domain.Account;
import com.example.HW1.domain.Customer;
import com.example.HW1.service.AccountService;
import com.example.HW1.service.CustomerService;
import com.example.HW1.service.DefaultAccountService;
import com.example.HW1.service.DefaultCustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/customers")
public class RestCustomerController {

    private final CustomerService customerService;
    private final AccountService accountService;


    public RestCustomerController(DefaultCustomerService defaultCustomerService, DefaultAccountService defaultAccountService) {

        this.customerService = defaultCustomerService;
        this.accountService = defaultAccountService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getOne(@PathVariable("id") Long id) {

        try {
            customerService.getOne(id);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body("Cant find account");
        }
    }

    @GetMapping
    public List<Customer> findAll() {

        return customerService.findAll();
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Customer customer) {

        try {
            customerService.save(customer);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body("Cant save new account");
        }
    }

    @PostMapping("/{id}")
    public ResponseEntity<?> edit(@PathVariable("id") Long id) {

        try {
            Customer customer = customerService.getOne(id);
            customerService.edit(customer);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body("Cant find account");
        }
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {

        try {
            customerService.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body("Cant delete account");
        }
    }

    @PutMapping("/account/{id}")
    public ResponseEntity<?> addAccount(@PathVariable Long id, @RequestBody Account account) {

        Customer customer = customerService.getOne(id);

        if (account.getNumber() == "") {

            Account newAccount = new Account();
            newAccount.setCurrency(account.getCurrency());
            newAccount.setCustomer(customer);
            accountService.save(newAccount);

            List<Account> customerAccounts = customer.getAccounts();
            customerAccounts.add(newAccount);
            customer.setAccounts(customerAccounts);

            customerService.edit(customer);

        } else {
            UUID number = UUID.fromString(account.getNumber());
            System.out.println(number);
            Optional<Account> accountOptional = accountService.findAll().stream().filter(el -> el.getNumber().equals(number.toString())).findAny();
            if (accountOptional.isPresent()) {
                Account accountFromOptional = accountOptional.get();
                Account account1 = accountService.getOne(accountFromOptional.getId());
                account1.setCustomer(customer);
                List<Account> customerAccounts = customer.getAccounts();
                customerAccounts.add(account1);
                customer.setAccounts(customerAccounts);
                System.out.println(customer.getAccounts());
                customerService.edit(customer);

                accountService.update(account1);
            } else {
                return ResponseEntity.badRequest().body("Incorrect account number");
            }
        }

        return ResponseEntity.ok(customer);


    }


    @DeleteMapping("/account/{id}")
    public ResponseEntity<?> deleteAccount(@PathVariable Long id, @RequestBody Account account) {

        if (accountService.getOne(id) == null) {
            return ResponseEntity.badRequest().body("Account does not exist");
        }
        Customer customer = customerService.getOne(id);

        List<Account> customerAccounts = customer.getAccounts();

        if (!customerAccounts.contains(accountService.getOne(account.getId()))) {
            return ResponseEntity.badRequest().body("This account isn't in the list ");
        }
        Account account1 = accountService.getOne(account.getId());
        account1.setCustomer(null);
        customerAccounts.remove(accountService.getOne(account.getId()));
        customer.setAccounts(customerAccounts);

        customerService.edit(customer);
        accountService.update(account1);
        return ResponseEntity.ok(customer);
    }


}
