package com.example.HW1.service;

import com.example.HW1.domain.Account;

import java.util.List;

public interface AccountService {

    public Account save(Account account);

    public boolean delete(Account account);

    public void deleteAll(List<Account> entities);

    public void saveAll(List<Account> entities);

    public List<Account> findAll();

    public boolean deleteById(long id);

    public Account getOne(long id);

    void update(Account account);
}
