package com.example.HW1.service;

import com.example.HW1.domain.Account;
import com.example.HW1.domain.Customer;

import java.util.List;

public interface CustomerService {

    public Customer save(Customer customer);

    public boolean delete(Customer customer);

    public void deleteAll(List<Customer> entities);

    public void saveAll(List<Customer> entities);

    public List<Customer> findAll();

    public boolean deleteById(long id);

    public Customer getOne(long id);

    public void edit(Customer customer);
}

