package com.example.HW1.service;

import com.example.HW1.dao.AccountDao;
import com.example.HW1.dao.Dao;
import com.example.HW1.domain.Account;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultAccountService implements AccountService {

    private Dao<Account> dao;
    private AccountDao accountDao;

    public DefaultAccountService(AccountDao accountDao) {

        this.dao = accountDao;
    }

    @Override
    public Account save(Account account) {

        return dao.save(account);
    }

    @Override
    public boolean delete(Account account) {

        return dao.delete(account);
    }

    @Override
    public void deleteAll(List<Account> entities) {

        dao.deleteAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {

        dao.saveAll(entities);
    }

    @Override
    public List<Account> findAll() { //done

        return dao.findAll();
    }

    @Override
    public boolean deleteById(long id) {

        return dao.deleteById(id);
    }

    @Override
    public Account getOne(long id) {

        return dao.getOne(id);
    }

    @Override
    public void update(Account account) {
        accountDao.update(account);
    }
}
