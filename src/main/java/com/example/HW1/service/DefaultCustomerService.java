package com.example.HW1.service;

import com.example.HW1.dao.CustomerDao;
import com.example.HW1.dao.Dao;
import com.example.HW1.domain.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultCustomerService implements CustomerService {

    private Dao<Customer> dao;
    private CustomerDao customerDao;
    public DefaultCustomerService (CustomerDao customerDao){
        this.dao = customerDao;
    }

    @Override
    public Customer save(Customer customer) {

        return dao.save(customer);
    }

    @Override
    public boolean delete(Customer customer) {

        return dao.delete(customer);
    }

    @Override
    public void deleteAll(List<Customer> entities) {
        dao.deleteAll(entities);
    }

    @Override
    public void saveAll(List<Customer> entities) {
        dao.saveAll(entities);
    }

    @Override
    public List<Customer> findAll() {

        return dao.findAll();
    }

    @Override
    public boolean deleteById(long id) {

        return dao.deleteById(id);
    }

    @Override
    public Customer getOne(long id) {

        return dao.getOne(id);
    }

    @Override
    public void edit(Customer customer) {
        customerDao.edit(customer);
    }
}
